#include "mesh.h"
#include "Vertex.h"

class KDNode {
public:
	BoundingBox bbox;
	KDNode* left;
	KDNode* right;
	std::vector<Triangle*> triangles;

	KDNode();

	KDNode* build(std::vector<Triangle*>& tris, int depth) const;
};

class BoundingBox {
private:
	Mesh mesh;
public:
	// min = minium coordinate of the box
	// max = maxium coordinate of the box
	Vec3Df min;
	Vec3Df max;

	void getBoundingBox(Triangle triangle){
		for (int i = 0; i < 3; i++){
			Vertex vert = mesh.vertices[triangle.v[i]];
			if (vert.p[0] < min.p[0])
				min.p[0] = vert.p[0];
			if (vert.p[1] < min.p[1])
				min.p[1] = vert.p[1];
			if (vert.p[2] < min.p[2])
				min.p[2] = vert.p[2];

			if (vert.p[0] > max.p[0])
				max.p[0] = vert.p[0];
			if (vert.p[1] > max.p[1])
				max.p[1] = vert.p[1];
			if (vert.p[2] > max.p[2])
				max.p[2] = vert.p[2];
		}
	}

	BoundingBox(){
		min = Vec3Df(100000, 100000, 100000);
		max = Vec3Df(-100000, -100000, -100000);
	}

	BoundingBox(Mesh aMesh){
		BoundingBox();
		mesh = aMesh;
	}

	BoundingBox(Mesh aMesh, Triangle triangle){
		BoundingBox();
		mesh = aMesh;
		getBoundingBox;
	}

};