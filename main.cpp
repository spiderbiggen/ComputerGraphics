#ifdef WIN32
#include <windows.h>
#endif
#include <GL/glut.h>
#include <stdlib.h>
#include <math.h>
#include <assert.h>
#include "raytracing.h"
#include "mesh.h"
#include "traqueboule.h"
#include "imageWriter.h"
#include <time.h>
#include <thread>
#include <atlstr.h>


//This is the main application
//Most of the code in here, does not need to be modified.
//It is enough to take a look at the function "drawFrame",
//in case you want to provide your own different drawing functions



Vec3Df MyCameraPosition;

//MyLightPositions stores all the light positions to use
//for the ray tracing. Please notice, the light that is 
//used for the real-time rendering is NOT one of these, 
//but following the camera instead.
std::vector<Vec3Df> MyLightPositions;

//Main mesh 
Mesh MyMesh; 

unsigned int WindowSize_X = 800;  // resolution X
unsigned int WindowSize_Y = 800;  // resolution Y

BoundingBox bbox;

//Setup an image with the size of the current image.
Image result(WindowSize_X, WindowSize_Y);
time_t start;
int row = 0;
Vec3Df origin00, dest00;
Vec3Df origin01, dest01;
Vec3Df origin10, dest10;
Vec3Df origin11, dest11;

const char* GROUPNAME = "The RAMpestampers";
const char* VERSION = "0.1.1";
const char* NAMES = "Timo, Daan, Michiel, Peter, Iwan, Job, Sytze and Stefan";
/**
 * Main function, which is drawing an image (frame) on the screen
*/
void drawFrame( )
{
	yourDebugDraw();
}

//animation is called for every image on the screen once
void animate()
{
	MyCameraPosition=getCameraPosition();
	glutPostRedisplay();
}



void display(void);
void reshape(int w, int h);
void keyboard(unsigned char key, int x, int y);

/**
 * Main Programme
 */
int main(int argc, char** argv)
{
	#ifdef WIN32
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
	#endif
		printf("Computer Graphics Raytracer %s v.%s.\nBy %s\n", GROUPNAME, VERSION, NAMES);
	#ifdef WIN32
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7);
	#endif

    glutInit(&argc, argv);

    //framebuffer setup
    glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH );

    // positioning and size of window
    glutInitWindowPosition(200, 100);
    glutInitWindowSize(WindowSize_X,WindowSize_Y);
    glutCreateWindow(argv[0]);	

    //initialize viewpoint
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glTranslatef(0,0,-4);
    tbInitTransform();     // This is for the trackball, please ignore
    tbHelp();             // idem
	MyCameraPosition=getCameraPosition();

	//activate the light following the camera
    glEnable( GL_LIGHTING );
    glEnable( GL_LIGHT0 );
    glEnable(GL_COLOR_MATERIAL);
    int LightPos[4] = {0,0,2,0};
    int MatSpec [4] = {1,1,1,1};
    glLightiv(GL_LIGHT0,GL_POSITION,LightPos);

	//normals will be normalized in the graphics pipeline
	glEnable(GL_NORMALIZE);
    //clear color of the background is black.
	glClearColor (0.075, 0.075, 0.075, 1.0);

	
	// Activate rendering modes
    //activate depth test
	glEnable( GL_DEPTH_TEST ); 
    //draw front-facing triangles filled
	//and back-facing triangles as wires
    glPolygonMode(GL_FRONT,GL_FILL);
    glPolygonMode(GL_BACK,GL_LINE);
    //interpolate vertex colors over the triangles
	glShadeModel(GL_SMOOTH);


	// glut setup... to ignore
    glutReshapeFunc(reshape);
    glutKeyboardFunc(keyboard);
    glutDisplayFunc(display);
    glutMouseFunc(tbMouseFunc);    // trackball
    glutMotionFunc(tbMotionFunc);  // uses mouse
    glutIdleFunc( animate);

	init();
	    
	//main loop for glut... this just runs your application
    glutMainLoop();
        
    return 0;  // execution never reaches this point
}

/**
 * OpenGL setup - functions do not need to be changed! 
 * you can SKIP AHEAD TO THE KEYBOARD FUNCTION
 */
//what to do before drawing an image
 void display(void)
{
	glPushAttrib(GL_ALL_ATTRIB_BITS);//store GL state
    // Effacer tout
    glClear( GL_COLOR_BUFFER_BIT  | GL_DEPTH_BUFFER_BIT); // clear image
    
    glLoadIdentity();  

    tbVisuTransform(); // init trackball

    drawFrame( );    //actually draw

    glutSwapBuffers();//glut internal switch
	glPopAttrib();//return to old GL state
}
//Window changes size
void reshape(int w, int h)
{
    glViewport(0, 0, (GLsizei) w, (GLsizei) h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    //glOrtho (-1.1, 1.1, -1.1,1.1, -1000.0, 1000.0);
    gluPerspective (50, (float)w/h, 0.01, 10);
    glMatrixMode(GL_MODELVIEW);
}


//transform the x, y position on the screen into the corresponding 3D world position
void produceRay(int x_I, int y_I, Vec3Df * origin, Vec3Df * dest)
{
		int viewport[4];
		double modelview[16];
		double projection[16];
		glGetDoublev(GL_MODELVIEW_MATRIX, modelview); //recuperer matrices
		glGetDoublev(GL_PROJECTION_MATRIX, projection); //recuperer matrices
		glGetIntegerv(GL_VIEWPORT, viewport);//viewport
		int y_new = viewport[3] - y_I;

		double x, y, z;
		
		gluUnProject(x_I, y_new, 0, modelview, projection, viewport, &x, &y, &z);
		origin->p[0]=float(x);
		origin->p[1]=float(y);
		origin->p[2]=float(z);
		gluUnProject(x_I, y_new, 1, modelview, projection, viewport, &x, &y, &z);
		dest->p[0]=float(x);
		dest->p[1]=float(y);
		dest->p[2]=float(z);
}

// react to keyboard input
void keyboard(unsigned char key, int x, int y)
{
    printf("key %c pressed at %d,%d\n",key,x,y);
    fflush(stdout);
    switch (key)
    {
	//add/update a light based on the camera position.
	case 'L':
		MyLightPositions.push_back(getCameraPosition());
		break;
	case 'l':
		MyLightPositions[MyLightPositions.size()-1]=getCameraPosition();
		break;
	case 'r':
	{
		//reset the rendered rows to 0 especially useful if you trace multiple time in a row
		row = 0;
		//Pressing r will launch the raytracing.
		cout << "Raytracing" << endl;

		//produce the rays for each pixel, by first computing
		//the rays for the corners of the frustum.
		produceRay(0, 0, &origin00, &dest00);
		produceRay(0, WindowSize_Y - 1, &origin01, &dest01);
		produceRay(WindowSize_X - 1, 0, &origin10, &dest10);
		produceRay(WindowSize_X - 1, WindowSize_Y - 1, &origin11, &dest11);
		//Store the startingtime
		start = time(0);
		//Create the boundingbox of this mesh
		bbox = BoundingBox(MyMesh);
		for each (Triangle tri in MyMesh.triangles) {
			bbox.expand(tri);
		}
		// get the amount of threads this pc supports
		unsigned int n = std::thread::hardware_concurrency();
		std::cout << n << " concurrent threads are supported." << std::endl;

		// support using up to 16 threads. Could be more but then we would be storing more and more than 8 is already very unlikely
		int threads = 1;
		std::thread t[16];
		if (n > 16)
			n = 16;
		for (int i = n; i > 0; i--){
			/*To make sure we don't check a line twice or accidentaly skip it
			 *calculate the number of threads we can use without needing to 
			 *run a non integer number of rows
			*/
			if (float(WindowSize_Y % i) == 0){
				threads = i;
				break;
			}
				
		}
		
		//Print some data about how we are running on this resolution
		std::cout << "Running with "<< threads << " threads." << std::endl;
		std::cout << float(WindowSize_Y)/float(threads) << " rows per thread" << std::endl;
		//Start the threads
		for (int i = 1; i < threads; i++){
			t[i] = std::thread(rayTraceThread, i, threads);
		}
		//Start the main thread
		rayTraceThread(0, threads);
		//Join the other threads into the main thread after they are all finished.
		for (int i = 1; i < threads; i++){
			t[i].join();
		}
	
		double seconds_since_start = difftime(time(0), start);

		#ifdef WIN32
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 6);
		#endif
			printf("\r100%% %.0f seconds.", seconds_since_start);
		#ifdef WIN32
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7);
		#endif
		cout << "\nFinished Raytracing in: " << seconds_since_start << " seconds." << endl;
		
		std::string string = getFileName();
		char *imgName = const_cast<char*>(string.c_str());
		cout << imgName<<endl;
		result.writeImage(imgName);
		break;
	}
	case 27:     // click ESC
        exit(0);
    }

	
	//produce the ray for the current mouse position
	Vec3Df testRayOrigin, testRayDestination;
	produceRay(x, y, &testRayOrigin, &testRayDestination);

	yourKeyboardFunc(key,x,y, testRayOrigin, testRayDestination);
}

const std::string getFileName() {
	time_t now = time(0);
	struct tm tstruct;
	char buffer[60];
	tstruct = *localtime(&now);
	strftime(buffer, sizeof(buffer), "%Y-%m-%d-%H-%M-%S", &tstruct);

	std::string result = std::string("results/") + buffer + std::string("-result.ppm");
	return result;
}

void rayTraceThread(int thread, int totalThreads){
	Vec3Df origin, dest;
	for (unsigned int y = 0; y < WindowSize_Y / totalThreads ; ++y){
		for (unsigned int x = 0; x < WindowSize_X; ++x)
		{
			//produce the rays for each pixel, by interpolating 
			//the four rays of the frustum corners.
			float xscale = 1.0f - float(x) / (WindowSize_X - 1);
			float yscale = 1.0f - float(y * totalThreads + thread) / (WindowSize_Y - 1);

			origin = yscale*(xscale*origin00 + (1 - xscale)*origin10) +
				(1 - yscale)*(xscale*origin01 + (1 - xscale)*origin11);
			dest = yscale*(xscale*dest00 + (1 - xscale)*dest10) +
				(1 - yscale)*(xscale*dest01 + (1 - xscale)*dest11);

			//launch raytracing for the given ray.
			Vec3Df rgb = performRayTracing(origin, dest, 0);
			//store the result in an image 	
			result.setPixel(x, (y * totalThreads + thread), RGBValue(rgb[0], rgb[1], rgb[2]));
		}
		double seconds = difftime(time(0), start);
		#ifdef WIN32
			SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 6);
		#endif
		printf("\r%.0f%% %.0f seconds.", float(row * 100 / WindowSize_Y), seconds);

		row++;
	}
}