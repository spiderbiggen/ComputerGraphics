#include <stdio.h>
#ifdef WIN32
#include <windows.h>
#endif
#include <GL/glut.h>
#include "raytracing.h"

//temporary variables
//these are only used to illustrate 
//a simple debug drawing. A ray 
Vec3Df testRayOrigin;
Vec3Df testRayDestination;
Vec3Df testLightPosition(1, 1, 1);
Vec3Df testLightPosition1(-2, 2, 2);
int currentLightSourceIndex = 0;

//use this function for any preprocessing of the mesh.
void init()
{
	//load the mesh file
	//please realize that not all OBJ files will successfully load.
	//Nonetheless, if they come from Blender, they should, if they 
	//are exported as WavefrontOBJ.

	//MyMesh.loadMesh("models/objectsMirror2.obj", true);
	//MyMesh.loadMesh("models/cube.obj", true);
    //MyMesh.loadMesh("models/cgsmooth.obj", true);
	//MyMesh.loadMesh("models/cube.obj", true);
	//MyMesh.loadMesh("models/cubes.obj", true);
	//MyMesh.loadMesh("models/Bunny2.obj", true);
	//MyMesh.loadMesh("models/Bunny3.obj", true);
	MyMesh.loadMesh("models/dodgeColorTest.obj", true);
	//MyMesh.loadMesh("models/monkeyConeMirrorHD.obj", true);
	//MyMesh.loadMesh("models/spheresHD.obj", true);

	MyMesh.computeVertexNormals();

	//one first move: initialize the first light source
	//at least ONE light source has to be in the scene!!!
	//here, we set it to the current location of the camera
	MyLightPositions.push_back(testLightPosition);

	//Write the amount of triangles in the loaded mesh to the console.
	std::cout << "Our mesh contains this many triangles: " << MyMesh.triangles.size() << "\n";

}

//return the color of your pixel.
Vec3Df performRayTracing(const Vec3Df origin, const Vec3Df dest, int reflections)
{

	//Will eventually contain the coordinates of our intersection point if there is one
	Vec3Df intersectCoordinates; 

	//Get the closest data from the possible intersections
	std::pair<float, int> intersectData = closestIntersect(origin, dest); 

	//if the triangleindex is equal to -1 then we do not have an intersection
	if (intersectData.second != -1){
		//Calculates the intersect point
		intersectCoordinates = origin + intersectData.first * dest;

		//return the color of the triangle at the intersect point
		return getColor(intersectData.second, intersectCoordinates, dest, reflections); //we return the diffuse of the material of the closest triangle. Which is the diffuse color.
	}
	else
		//Otherwise return a darkgrey background
		return Vec3Df(0.075, 0.075, 0.075);
	
}

//Finds the closest triangle that intersects with the ray from origin to dest
std::pair<float, int> closestIntersect(const Vec3Df origin, const Vec3Df dest){
	float smallestDistance = FLT_MAX;
	int triangleIndex = -1;
	Vec3Df something;
	if (bbox.insideBox(origin, dest)){
		for (int i = 0; i < MyMesh.triangles.size(); ++i){
			//The triangle to try and intersect
			Triangle triangle = MyMesh.triangles[i];

			//Get the 3 vertices that make this triangle
			Vec3Df vertex1 = MyMesh.vertices[triangle.v[0]].p;
			Vec3Df vertex2 = MyMesh.vertices[triangle.v[1]].p;
			Vec3Df vertex3 = MyMesh.vertices[triangle.v[2]].p;

			//Some intermediary vectors
			Vec3Df tvec, pvec, qvec;

			/* 
			 * Det is the Determinant 
			 * inv_det is the inverse of the determinant
			 * u, v are barycentric coordinates
			 */
			float det, inv_det, u, v;

			// Find vectors for two edges sharing vertex1
			Vec3Df edge1 = vertex2 - vertex1;
			Vec3Df edge2 = vertex3 - vertex1;

			//Calculate the determinant
			pvec = Vec3Df::crossProduct(dest, edge2);
			det = Vec3Df::dotProduct(edge1, pvec);

			// if det is near zero the ray lies in the same plane as the triangle
			if (!(det > -0.00001 && det < 0.00001)){

				//calculate the inverse determinant
				inv_det = 1 / det;

				// Calculate the distance form vertex1 to the origin
				tvec = origin - vertex1;

				// Calculate u parameter and test bounds
				u = inv_det * (Vec3Df::dotProduct(tvec, pvec));
				if (!(u < 0.0 || u > 1.0)){

					// Calculate v parameter and test bounds
					qvec = Vec3Df::crossProduct(tvec, edge1);
					v = inv_det * Vec3Df::dotProduct(dest, qvec);
					if (!(v < 0.0 || u + v > 1.0)){

						// at this stage we can compute t to find out where the intersection point is on the line
						float t = inv_det * Vec3Df::dotProduct(edge2, qvec);

						if (t > 0.00001 && t < smallestDistance){
							triangleIndex = i;
							smallestDistance = t; //we save the smallest distance
						}
					}
				}
			}
		}
	}
	//return a pair of the distance t and the intersected triangle
	return std::make_pair(smallestDistance, triangleIndex);
}

//Calculate the normal of the triangle
Vec3Df getNormal(const Triangle triangle){
	Vec3Df res,u1, u2;

	//You need these 3 vertices to compute this.
	Vec3Df vertex1 = MyMesh.vertices[triangle.v[0]].p;
	Vec3Df vertex2 = MyMesh.vertices[triangle.v[1]].p;
	Vec3Df vertex3 = MyMesh.vertices[triangle.v[2]].p;

	//2 edges of the triangle
	u1 = vertex2 - vertex1;
	u2 = vertex3 - vertex1;

	//Calculate the normal of these two edges
	res = Vec3D<float>::crossProduct(u1, u2);
	//Normalize the normal
	res.normalize();
	return res;
}

//Returns the color at the intersection of the triangle
Vec3Df getColor(int triangleIndex, Vec3Df intersectCoordinates, Vec3Df dest, int reflections){


	//strores the intersected triangle
	Triangle triangleIntersect = MyMesh.triangles[triangleIndex];
	//stores the material of the triangle
	Material mat = MyMesh.materials[MyMesh.triangleMaterials[triangleIndex]];
	//Store the different parts of the eventual color
	Vec3Df diffuse = Vec3Df(0, 0, 0);
	Vec3Df specular = Vec3Df(0, 0, 0);
	Vec3Df ambient = 0.3 * mat.Kd();
	//The eventual color
	Vec3Df resultColor;

	//Loop over all the lights
	for each (Vec3Df light in MyLightPositions) {
		//Calculate a vector from the intersection to the light
		Vec3Df lightDest = light - intersectCoordinates;
		//Check for an intersection
		std::pair<float, int> intersectData = closestIntersect(intersectCoordinates, lightDest);
		//Check if there is something between the light and the intersectionpoint
		if (intersectData.first > 1.0){
			//Normalize the light vector so that light from far away isn't brighter :O
			light.normalize();
			//Calculate the vector that is the bisector between the light ray and the view ray
			Vec3Df Bisector = (-intersectCoordinates + light);
			Bisector.normalize();
			//get the Dotproduct of the bisector and the normal, needs to be > 0
			float HdotN = max(0.0, Vec3D<float>::dotProduct(Bisector, getNormal(triangleIntersect)));

			//hardness should be stored in the material but isn't so just always use 1
			float hardness = 1;
			//calculate the specular and diffuse colors according to blinn-phong
			specular += 1.0 / float(MyLightPositions.size()) * mat.Ks() * pow(HdotN, hardness);
			diffuse += 1.0 / float(MyLightPositions.size()) * mat.Kd() * max(0.0, Vec3D<float>::dotProduct(getNormal(triangleIntersect), light));
		}
	}
	//add the 3 color layers
	resultColor = ambient + diffuse + specular;
	//clip the values, 0 <= x <= 1
	resultColor.clip();

	//if the material is mirror like, we should calculate a new ray from the intersection to the new destination.
	if (isMirror(mat) && reflections < 4) {
		Vec3Df newDirection = calculateReflection(dest, triangleIntersect, mat);
		Vec3Df newOrigin = intersectCoordinates;
		Vec3Df reflection = performRayTracing(newOrigin, newDirection, reflections++);
		resultColor = resultColor*(Vec3Df(1.0,1.0,1.0)-mat.Ks()) + reflection*mat.Ks();
	}

	// return the final color 
	return resultColor;
}

//does not work yet
Vec3Df calculateReflection(Vec3Df directionOrigin, Triangle triangle, Material material){

	//Calculate the reflected ray
	float nCrossv = Vec3Df::dotProduct(getNormal(triangle), directionOrigin);
	Vec3Df res = directionOrigin - 2 * nCrossv* getNormal(triangle)*material.Ks();
	//return the reflected ray;
	return res;
}
//returns true iff a material is somewhat like a mirror
boolean isMirror(Material material){
	return material.Ks()[2]>0.93;
}

void yourDebugDraw()
{
	//draw open gl debug stuff
	//this function is called every frame

	//let's draw the mesh
	MyMesh.draw();
	
	//let's draw the lights in the scene as points
	glPushAttrib(GL_ALL_ATTRIB_BITS); //store all GL attributes
	glDisable(GL_LIGHTING);
	glColor3f(1,1,1);
	glPointSize(10);
	glBegin(GL_POINTS);
	for (int i=0;i<MyLightPositions.size();++i)
		glVertex3fv(MyLightPositions[i].pointer());
	glEnd();
	glPopAttrib();//restore all GL attributes
	//The Attrib commands maintain the state. 
	//e.g., even though inside the two calls, we set
	//the color to white, it will be reset to the previous 
	//state after the pop.


	//as an example: we draw the test ray, which is set by the keyboard function
	glPushAttrib(GL_ALL_ATTRIB_BITS);
	glDisable(GL_LIGHTING);
	glBegin(GL_LINES);
	glColor3f(0,1,1);
	glVertex3f(testRayOrigin[0], testRayOrigin[1], testRayOrigin[2]);
	glColor3f(0,0,1);
	glVertex3f(testRayDestination[0], testRayDestination[1], testRayDestination[2]);
	glEnd();
	glPointSize(10);
	glBegin(GL_POINTS);
	glVertex3fv(MyLightPositions[0].pointer());
	glEnd();
	glPopAttrib();
	
	//draw whatever else you want...
	//glutSolidSphere(1,10,10);
	////allows you to draw a sphere at the origin.
	////using a glTranslate, it can be shifted to whereever you want
	////if you produce a sphere renderer, this 
	////triangulated sphere is nice for the preview
}

//yourKeyboardFunc is used to deal with keyboard input.
//t is the character that was pressed
//x,y is the mouse position in pixels
//rayOrigin, rayDestination is the ray that is going in the view direction UNDERNEATH your mouse position.
//
//A few keys are already reserved: 
//'L' adds a light positioned at the camera location to the MyLightPositions vector
//'l' modifies the last added light to the current 
//    camera position (by default, there is only one light, so move it with l)
//    ATTENTION These lights do NOT affect the real-time rendering. 
//    You should use them for the raytracing.
//'r' calls the function performRaytracing on EVERY pixel, using the correct associated ray. 
//    It then stores the result in an image "result.ppm".
//    Initially, this function is fast (performRaytracing simply returns 
//    the target of the ray - see the code above), but once you replaced 
//    this function and raytracing is in place, it might take a 
//    while to complete...
void yourKeyboardFunc(char t, int x, int y, const Vec3Df & rayOrigin, const Vec3Df & rayDestination)
{
	//Vec3Df direction = (rayDestination[0] - rayOrigin[0], rayDestination[1] - rayOrigin[1], rayDestination[2] - rayOrigin[2]);
	//here, as an example, I use the ray to fill in the values for my upper global ray variable
	//I use these variables in the debugDraw function to draw the corresponding ray.
	//try it: Press a key, move the camera, see the ray that was launched as a line.
	testRayOrigin=rayOrigin;	
	testRayDestination=rayDestination;
	boolean lightOperation = false;

	// do here, whatever you want with the keyboard input t.
	switch (t){
	case 'w': MyLightPositions[currentLightSourceIndex][0] += 0.1;
		lightOperation = true;
		break;
	case 's': MyLightPositions[currentLightSourceIndex][0] += -0.1;
		lightOperation = true;
		break;
	case 'a': MyLightPositions[currentLightSourceIndex][1] += 0.1;
		lightOperation = true;
		break;
	case 'd': MyLightPositions[currentLightSourceIndex][1] += -0.1;
		lightOperation = true;
		break;
	case 'q': MyLightPositions[currentLightSourceIndex][2] += 0.1;
		lightOperation = true;
		break;
	case 'e': MyLightPositions[currentLightSourceIndex][2] += -0.1;
		lightOperation = true;
		break;
	case '1': if (currentLightSourceIndex < MyLightPositions.size() - 1);
		currentLightSourceIndex++;
		lightOperation = true;
		std::cout <<"Moved to light source index: " << currentLightSourceIndex << std::endl;
		break;
	case '2':if (currentLightSourceIndex>0)
		currentLightSourceIndex--;
		lightOperation = true;
		std::cout << "Moved to light source index: " << currentLightSourceIndex << std::endl;
		break;
	
	}
	if (lightOperation){
		std::cout << "current position of light source: (" << MyLightPositions[currentLightSourceIndex][0] << ", " << MyLightPositions[currentLightSourceIndex][1] << ", " << MyLightPositions[currentLightSourceIndex][2] << ')' << std::endl;
	}
	//...
	
	
	//std::cout<<t<<" pressed! The mouse was in location "<<x<<","<<y<<"!"<<std::endl;	
}

