#ifndef RAYTRACING_Hjdslkjfadjfasljf
#define RAYTRACING_Hjdslkjfadjfasljf
#include <vector>
#include "mesh.h"

//Welcome to your MAIN PROJECT...
//THIS IS THE MOST relevant code for you!
//this is an important file, raytracing.cpp is what you need to fill out
//In principle, you can do the entire project ONLY by working in these two files

extern Mesh MyMesh; //Main mesh
extern std::vector<Vec3Df> MyLightPositions;
extern Vec3Df MyCameraPosition; //currCamera
extern unsigned int WindowSize_X;//window resolution width
extern unsigned int WindowSize_Y;//window resolution height
extern unsigned int RayTracingResolutionX;  // largeur fenetre
extern unsigned int RayTracingResolutionY;  // largeur fenetre
extern BoundingBox bbox;

//use this function for any preprocessing of the mesh.
void init();

//you can use this function to transform a click to an origin and destination
//the last two values will be changed. There is no need to define this function.
//it is defined elsewhere
void produceRay(int x_I, int y_I, Vec3Df & origin, Vec3Df & dest);

//this returns the distance and the triangleindex if the triangle and ray intersect
std::pair<float, int> closestIntersect(const Vec3Df origin, const Vec3Df dest);

//your main function to rewrite
Vec3Df performRayTracing(const Vec3Df origin, const Vec3Df dest, int reflections);

//a function to debug --- you can draw in OpenGL here
void yourDebugDraw();

//Calculates the reflection ray
Vec3Df calculateReflection(Vec3Df directionOrigin, Triangle triangle, Material material);

//want keyboard interaction? Here it is...
void yourKeyboardFunc(char t, int x, int y, const Vec3Df & rayOrigin, const Vec3Df & rayDestination);

//checks if the material is a mirror
boolean isMirror(Material material);

//Function to run the raytracing on multiple threads
void rayTraceThread(int thread, int threads);

//Calculates the normal of a triangle
Vec3Df getNormal(const Triangle triangle);

//Calculates the color of a specific point.
Vec3Df getColor(int triangleIndex, Vec3Df intersectCoordinates, Vec3Df dest, int reflections);

//give every file a unique name based on the date.
const std::string getFileName();
#endif